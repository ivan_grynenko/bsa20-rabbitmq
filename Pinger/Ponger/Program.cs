﻿using System;
using System.Configuration;
using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.Additives;
using RabbitMQ.Wrapper.Models;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ponger is starting ...\nTo stop press any key");

            var settings = new ListenerSettings
            {
                AutoAcknowledge = Convert.ToBoolean(ConfigurationManager.AppSettings["AutoAcknowledge"]),
                ExchangeName = ConfigurationManager.AppSettings["ExchangeName"],
                ListeningQueueName = ConfigurationManager.AppSettings["ListeningQueueName"],
                PublishingQueueName = ConfigurationManager.AppSettings["PublishingQueueName"]
            };

            RabbitService service = new RabbitService("pong", new MessageQueue(new RabbitConnectionFactory(ConfigurationManager.AppSettings["HostName"])), settings);
            service.ListenQueue();
            service.SendMessageToQueue();

            Console.ReadKey();
        }
    }
}
