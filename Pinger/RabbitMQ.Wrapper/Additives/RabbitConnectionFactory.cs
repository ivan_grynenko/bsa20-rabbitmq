﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper.Additives
{
    public class RabbitConnectionFactory : IRabbitConnectionFactory
    {
        private string hostName;

        public RabbitConnectionFactory(string hostName)
        {
            this.hostName = hostName;
        }

        public IConnection CreateConnection()
        {
            var factory = new ConnectionFactory() { HostName = hostName };
            return factory.CreateConnection();
        }
    }
}
