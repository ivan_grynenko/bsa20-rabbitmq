﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using System;

namespace RabbitMQ.Wrapper.Additives
{
    public class MessageQueue : IDisposable, IMessageQueue
    {
        public IModel Channel { get; set; }
        private readonly IConnection connection;

        public MessageQueue(IRabbitConnectionFactory factory)
        {
            connection = factory.CreateConnection();
            Channel = connection.CreateModel();
        }

        public void DeclareExchange(string exchangeName, string exchangeType = "direct")
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType);
        }

        public void BindQueue(string queueName, string exhangeName, string routingKey)
        {
            Channel.QueueDeclare(queue: queueName,
                                 durable: true,
                                 autoDelete: false,
                                 exclusive: false);
            Channel.QueueBind(queue: queueName,
                              exchange: exhangeName,
                              routingKey: routingKey);
        }

        public void SetBasicConsume(string queueName, bool autoAck, IBasicConsumer consumer)
        {
            Channel.BasicConsume(queue: queueName,
                                 autoAck: false,
                                 consumer: consumer);
        }

        public void SetBasicPublish(string exchangeName, string routingKey, IBasicProperties basicProperties, ReadOnlyMemory<byte> body)
        {
            Channel.BasicPublish(exchange: exchangeName,
                                 routingKey: routingKey,
                                 basicProperties: basicProperties,
                                 body: body);
        }

        public void Dispose()
        {
            connection?.Dispose();
            Channel?.Dispose();
        }
    }
}
