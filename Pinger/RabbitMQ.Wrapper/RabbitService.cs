﻿using System;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper
{
    public class RabbitService : IDisposable
    {
        private IMessageQueue messageQueue;
        private IListenerSettings listenerSettings;
        private string pingOrPong;
        private Random random = new Random();
        private int score;

        public RabbitService(string pingOrPong, IMessageQueue messageQueue, IListenerSettings listenerSettings)
        {
            this.pingOrPong = pingOrPong;
            this.messageQueue = messageQueue;
            this.listenerSettings = listenerSettings;
            messageQueue.DeclareExchange(listenerSettings.ExchangeName, ExchangeType.Direct);
            listenerSettings.Consumer = new EventingBasicConsumer(messageQueue.Channel);
        }

        public void ListenQueue()
        {
            messageQueue.BindQueue(queueName: listenerSettings.ListeningQueueName,
                                  exhangeName: listenerSettings.ExchangeName,
                                  routingKey: pingOrPong == "ping" ? "pong" : "ping");

            listenerSettings.Consumer.Received += Responde;

            messageQueue.SetBasicConsume(queueName: listenerSettings.ListeningQueueName,
                                         autoAck: listenerSettings.AutoAcknowledge,
                                         consumer: listenerSettings.Consumer);
        }

        public void SendMessageToQueue()
        {
            byte[] body;

            if (CanReturn())  body = Encoding.UTF8.GetBytes(pingOrPong);
            else body = Encoding.UTF8.GetBytes("lost");

            messageQueue.SetBasicPublish(exchangeName: listenerSettings.ExchangeName,
                                         routingKey: pingOrPong,
                                         basicProperties: null,
                                         body: body);
        }

        private void Responde(object sender, BasicDeliverEventArgs arg)
        {
            var body = Encoding.UTF8.GetString(arg.Body.ToArray());

            if (body == "lost")
                body = $"Woohoo! The scrore is {++score}. I'm serving!";

            var message = DateTime.Now + "\nMessage: " + body;
            Console.WriteLine(message);
            messageQueue.Channel.BasicAck(arg.DeliveryTag, false);
            Thread.Sleep(2500);
            SendMessageToQueue();
        }

        private bool CanReturn() => random.Next(0, 10) > 2 ? true : false;

        public void Dispose() => messageQueue.Dispose();
    }
}
