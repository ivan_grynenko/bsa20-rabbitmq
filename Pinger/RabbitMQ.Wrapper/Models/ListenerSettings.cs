﻿using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper.Models
{
    public class ListenerSettings : IListenerSettings
    {
        public bool AutoAcknowledge { get; set; }
        public EventingBasicConsumer Consumer { get; set; }
        public string ListeningQueueName { get; set; }
        public string PublishingQueueName { get; set; }
        public string ExchangeName { get; set; }
    }
}
