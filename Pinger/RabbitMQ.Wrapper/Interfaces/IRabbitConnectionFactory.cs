﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IRabbitConnectionFactory
    {
        IConnection CreateConnection();
    }
}