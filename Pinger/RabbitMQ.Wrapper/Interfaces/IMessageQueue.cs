﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageQueue
    {
        IModel Channel { get; set; }

        void BindQueue(string queueName, string exhangeName, string routingKey);
        void DeclareExchange(string exchangeName, string exchangeType = "direct");
        void Dispose();
        void SetBasicConsume(string queueName, bool autoAck, IBasicConsumer consumer);
        void SetBasicPublish(string exchangeName, string routingKey, IBasicProperties basicProperties, ReadOnlyMemory<byte> body);
    }
}