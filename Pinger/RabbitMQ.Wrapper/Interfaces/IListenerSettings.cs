﻿using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IListenerSettings
    {
        bool AutoAcknowledge { get; set; }
        EventingBasicConsumer Consumer { get; set; }
        string ExchangeName { get; set; }
        string ListeningQueueName { get; set; }
        string PublishingQueueName { get; set; }
    }
}